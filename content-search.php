<?php
/**
 * @package Golf Tripster
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  
  <?php if(get_post_thumbnail_id($post->ID)): ?>
  <div class="feature-post-wrapper">
    <img class="post-feature-image" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>">
  </div>
  <?php endif; ?>
  
	<header class="post-header">
		<h1 class="entry-title category-post"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
    
			  
		<span style="font-size:12px;"><!-- Posted <?php echo get_the_date(); ?> --> Written by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author_meta('display_name'); ?></a></span>
			
			  <?php /* ?>
			  <div class="cf">
  			<a class="pull-left" href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" style="margin:0 10px 10px 0;">
          <?php echo get_avatar(get_the_author_meta('ID'), 50); ?>
        </a>
        </div>
        <?php */ ?>     
      
      
	</header><!-- .entry-header -->

	<div class="entry-content">
	  
	  
		<?php echo do_shortcode(wp_trim_words($post->post_content, 90, ' ')); ?>	<a href="<?php the_permalink(); ?>" class="more-link">More&nbsp;&gt;</a>
		<?php include('social-snippet.php'); ?>
		

		
    
	</div><!-- .entry-content -->

</article><!-- #post-## -->
<hr>