<?php
/**
 * @package Golf Tripster
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>

		<div class="entry-meta">
			<?php //golftripster_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
	  
		 <?php if($slideshow = get_field('slideshow')): 
  		    $first_image_thumb = wp_get_attachment_image_src( $slideshow[0]['image'], 'thumbnail' );
  		    $first_image = wp_get_attachment_image_src( $slideshow[0]['image'], 'full' );
  		  ?>
  		<div class="course-take-slideshow">

  		  <a href="<?php echo $first_image[0]; ?>" data-ob="lightbox[<?php echo $post->post_name; ?>]" data-ob_caption="<?php echo $slideshow[0]['description'] ?>">
  		    <img src="<?php echo $first_image_thumb[0]; ?>" width="97%">
  		  </a>

  		  <?php foreach($slideshow as $index => $slide): 

  		      $image = wp_get_attachment_image_src( $slide['image'], 'full' );
  		  ?>
  		    <a class="more-link" href="<?php echo $image[0]; ?>" data-ob="lightbox[<?php echo $post->post_name; ?>]" data-ob_caption="<?php echo $slide['description'] ?>"><?php if($index == 0) :?><strong>View Slideshow</strong><?php endif; ?></a>
  		  <?php endforeach; ?>

  		</div>
  		<?php endif; ?>

  		<?php the_content(); ?>
  		
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'golftripster' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-meta">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$category_list = get_the_category_list( __( ', ', 'golftripster' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'golftripster' ) );

			if ( ! golftripster_categorized_blog() ) {
				// This blog only has 1 category so we just need to worry about tags in the meta text
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'golftripster' );
				} else {
					$meta_text = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'golftripster' );
				}

			} else {
				// But this blog has loads of categories so we should probably display them here
				if ( '' != $tag_list ) {
					$meta_text = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'golftripster' );
				} else {
					$meta_text = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'golftripster' );
				}

			} // end check for categories on this blog

			printf(
				$meta_text,
				$category_list,
				$tag_list,
				get_permalink(),
				the_title_attribute( 'echo=0' )
			);
		?>

    <?php if (get_field('affiliate_link')): ?>
			<div class="aff-link-wrapper">
				<a href="<?php echo get_field('affiliate_link'); ?>" target="_blank">Buy on Amazon</a>
			</div>
			<div style="clear:right;"></div>
		<?php endif ?>

    <?php include('social-snippet.php'); ?>
    
		<?php edit_post_link( __( 'Edit', 'golftripster' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
