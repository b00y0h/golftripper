<?php if(!get_post_meta($post->ID, 'no_social', true)): ?>
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style">
<a class="addthis_button_facebook_like" fb:like:layout="button_count" addthis:url="<?php echo the_permalink(); ?>" addthis:title="<?php echo the_title(); ?>"></a>
<a class="addthis_button_tweet" addthis:url="<?php echo the_permalink(); ?>" addthis:title="<?php echo the_title(); ?>"></a>
<a class="addthis_button_google_plusone" g:plusone:size="medium" addthis:url="<?php echo the_permalink(); ?>" addthis:title="<?php echo the_title(); ?>"></a>
<a class="addthis_counter addthis_pill_style" addthis:url="<?php echo the_permalink(); ?>" addthis:title="<?php echo the_title(); ?>"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51ba5d4a1ecdec02"></script>
<!-- AddThis Button END -->
<?php endif; ?>