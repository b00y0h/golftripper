<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> style="position:relative;">
	<header class="top-100-header">
		<h1 class="top-100-title"><?php the_title(); ?> (<?php the_field('years'); ?>)</h1>
	</header><!-- .entry-header -->

	<div class="entry-content">
    
    <div class="top-100-description">
      <?php the_field('description'); ?>
    </div>
		
		<ul class="top100-list">
		   <?php
		   	$course_rankings = get_course_by_ranking($post->post_name);
        $rankings = get_top100_rankings(100);
        foreach($rankings as $rank=>$data): 
          $reviewed_course = array_search($rank, $course_rankings);
          ?>
        <li>
          <?php if($reviewed_course): ?>
          <a href="<?php echo get_permalink($reviewed_course); ?>">
            <strong><?php echo $data['course']; ?></strong>
          </a><br>
          <?php else: ?>
            <strong><?php echo $data['course']; ?></strong><br>
          <?php endif; ?>
          <span class="subtext"><?php echo $data['info']; ?></span><br>         
        </li>
        <?php endforeach; ?>
		</ul>
		
		
	</div><!-- .entry-content -->
	<?php include('social-snippet.php'); ?>
	<?php edit_post_link( __( 'Edit', 'golftripster' ), '<footer class="entry-meta"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->