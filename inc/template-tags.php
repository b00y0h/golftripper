<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package Golf Tripster
 */

if ( ! function_exists( 'xold_golftripster_content_nav' ) ) :
/**
 * Display navigation to next/previous pages when applicable
 */
function xold_golftripster_content_nav( $nav_id ) {
	global $wp_query, $post;

	// Don't print empty markup on single pages if there's nowhere to navigate.
	if ( is_single() ) {
		$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
		$next = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous )
			return;
	}

	// Don't print empty markup in archives if there's only one page.
	if ( $wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() ) )
		return;

	$nav_class = ( is_single() ) ? 'navigation-post' : 'navigation-paging';

	?>
	<nav role="navigation" id="<?php echo esc_attr( $nav_id ); ?>" class="<?php echo $nav_class; ?>">
		<h1 class="screen-reader-text"><?php _e( 'Post navigation', 'golftripster' ); ?></h1>

	<?php if ( is_single() ) : // navigation links for single posts ?>

		<?php previous_post_link( '<div class="nav-previous">%link</div>', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'golftripster' ) . '</span> %title' ); ?>
		<?php next_post_link( '<div class="nav-next">%link</div>', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'golftripster' ) . '</span>' ); ?>

	<?php elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ) : // navigation links for home, archive, and search pages ?>

		<?php if ( get_next_posts_link() ) : ?>
		<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'golftripster' ) ); ?></div>
		<?php endif; ?>

		<?php if ( get_previous_posts_link() ) : ?>
		<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'golftripster' ) ); ?></div>
		<?php endif; ?>

	<?php endif; ?>

	</nav><!-- #<?php echo esc_html( $nav_id ); ?> -->
	<?php
}
endif; // golftripster_content_nav

if ( ! function_exists( 'golftripster_content_nav' ) ):

function golftripster_content_nav( $id, $scope = 2) {
  // custom pagination
     	global $wp, $posts_per_page, $page, $wp_query;    
     	$mypage = $page;

  	$numPages = ceil($wp_query->found_posts / $posts_per_page );

  
  	if( $numPages <= 1 )
  		return; // no need for pagination

  	$queryVars = $wp->query_vars;
 
  	$curPage = isset( $queryVars[ 'paged' ] ) ? (int) $queryVars[ 'paged' ] : 1;

  	// page bounds
  	$start = $curPage - $scope;
  	$end = $curPage + $scope;

  	// if we can't satisfy the scope (add enough pages) on one side,
  	// add pages to the other side
  	if( $start <= 1 ) {
  		$end += ( 1 - $start );
  		$start = 2;
  	}
  	else if( $end >= $numPages ) {
  		$start -= ( $end - $numPages );
  		$end = $numPages - 1;
  	}

  	// limit the start and end to their extreme values
  	$start = max( $start, 2 );
  	$end = min( $end, $numPages - 1 );

  	$pagesToLinkTo = array( 1 );
  	for( $page = $start; $page <= $end; $page++ )
  		$pagesToLinkTo[] = $page;
  	$pagesToLinkTo[] = $numPages;

  	$prevPage = (1 <= $curPage-1) ? $curPage-1 : 1;
  	
    $nextPage = ($numPages >= $curPage+1) ? $curPage+1 : $numPages;

    echo '<div id="' . $id . '" class="pagination pagination-centered blue">';
    echo '<ul>';

  	echo '<li><a href="' . update_pager_url($prevPage) . '">&lt;</a></li>';

  	foreach( $pagesToLinkTo as $page ) {
  		if( $page - $prevPage > 1 ) // skipped a few pages
  		echo '<li><span class="page-numbers dots">&hellip;</span></li>'; // add a spacer
      if ($curPage == $page) {
    		echo '<li class="active"><a href="' . update_pager_url($page) . '">' . $page . '</a></li>';      
      } else {
    		echo '<li><a href="' . update_pager_url($page) . '">' . $page . '</a></li>';
    }
  		$prevPage = $page;

  	}


  	echo '<li><a href="' . update_pager_url($nextPage) . '">&gt;</a></li>';

  	echo '</ul>';
    echo '</div>';
    
}
endif; // golftripper_content_nav



function update_pager_url($page_number){
  $bare_url = preg_replace('/page\/(\d+)\//','', $_SERVER['REQUEST_URI']);
  $output = $bare_url . 'page/' . $page_number .'/';
  return $output;
}

if ( ! function_exists( 'golftripster_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function golftripster_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'golftripster' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'golftripster' ), '<span class="edit-link">', '<span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<footer>
				<div class="comment-author vcard">
					<?php echo get_avatar( $comment, 40 ); ?>
					<?php printf( __( '%s <span class="says">says:</span>', 'golftripster' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
				</div><!-- .comment-author .vcard -->
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em><?php _e( 'Your comment is awaiting moderation.', 'golftripster' ); ?></em>
					<br />
				<?php endif; ?>

				<div class="comment-meta commentmetadata">
					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"><time datetime="<?php comment_time( 'c' ); ?>">
					<?php printf( _x( '%1$s at %2$s', '1: date, 2: time', 'golftripster' ), get_comment_date(), get_comment_time() ); ?>
					</time></a>
					<?php edit_comment_link( __( 'Edit', 'golftripster' ), '<span class="edit-link">', '<span>' ); ?>
				</div><!-- .comment-meta .commentmetadata -->
			</footer>

			<div class="comment-content"><?php comment_text(); ?></div>

			<div class="reply">
			<?php
				comment_reply_link( array_merge( $args,array(
					'depth'     => $depth,
					'max_depth' => $args['max_depth'],
				) ) );
			?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->

	<?php
			break;
	endswitch;
}
endif; // ends check for golftripster_comment()

if ( ! function_exists( 'golftripster_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function golftripster_posted_on() {
	printf( __( 'Posted on <a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a><span class="byline"> by <span class="author vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span>', 'golftripster' ),
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( __( 'View all posts by %s', 'golftripster' ), get_the_author() ) ),
		get_the_author()
	);
}
endif;
/**
 * Returns true if a blog has more than 1 category
 */
function golftripster_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );

		// Count the number of categories that are attached to the posts
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so golftripster_categorized_blog should return true
		return true;
	} else {
		// This blog has only 1 category so golftripster_categorized_blog should return false
		return false;
	}
}

/**
 * Flush out the transients used in golftripster_categorized_blog
 */
function golftripster_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', 'golftripster_category_transient_flusher' );
add_action( 'save_post', 'golftripster_category_transient_flusher' );