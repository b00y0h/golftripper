<?php
/**
 *  Install Add-ons
 *  
 *  The following code will include all 4 premium Add-Ons in your theme.
 *  Please do not attempt to include a file which does not exist. This will produce an error.
 *  
 *  All fields must be included during the 'acf/register_fields' action.
 *  Other types of Add-ons (like the options page) can be included outside of this action.
 *  
 *  The following code assumes you have a folder 'add-ons' inside your theme.
 *
 *  IMPORTANT
 *  Add-ons may be included in a premium theme as outlined in the terms and conditions.
 *  However, they are NOT to be included in a premium / free plugin.
 *  For more information, please read http://www.advancedcustomfields.com/terms-conditions/
 */ 

// Fields 
add_action('acf/register_fields', 'my_register_fields');

function my_register_fields()
{
	include_once(get_template_directory() . '/advanced-custom-fields/add-ons/acf-repeater/repeater.php');
	//include_once('add-ons/acf-gallery/gallery.php');
	//include_once('add-ons/acf-flexible-content/flexible-content.php');
}

// Options Page 
include_once( get_template_directory() . '/advanced-custom-fields/add-ons/acf-options-page/acf-options-page.php' );


/**
 *  Register Field Groups
 *
 *  The register_field_group function accepts 1 array which holds the relevant data to register a field group
 *  You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 */
/*
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_course-profile',
		'title' => 'Course Profile',
		'fields' => array (
			array (
				'key' => 'field_51924b9a977da',
				'label' => 'Course Logo',
				'name' => 'course_logo',
				'type' => 'image',
				'instructions' => 'Add course logo',
				'save_format' => 'url',
				'preview_size' => 'full',
			),
			array (
				'key' => 'field_5194f1248ae63',
				'label' => 'Scenic Image',
				'name' => 'scenic_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
			),
			array (
				'key' => 'field_51924bc8977db',
				'label' => 'Golf Digest Public Ranking',
				'name' => 'golf_digest_public_ranking',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_51924be5977dc',
				'label' => 'Golf Magazine Public Ranking',
				'name' => 'golf_magazine_public_ranking',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_51924bfe977dd',
				'label' => 'Golf Digest Ranking',
				'name' => 'golf_digest_ranking',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_51924c0d977de',
				'label' => 'Golf Magazine Ranking',
				'name' => 'golf_magazine_ranking',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'html',
			),
			array (
				'key' => 'field_51924d9dfd4a6',
				'label' => 'Address',
				'name' => 'address',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_51924dc8fd4a7',
				'label' => 'City',
				'name' => 'city',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_51924cdafd4a5',
				'label' => 'State',
				'name' => 'state',
				'type' => 'select',
				'instructions' => 'Select the state this course resides in.',
				'required' => 1,
				'multiple' => 0,
				'allow_null' => 0,
				'choices' => array (
					'' => '',
					'Alabama' => 'Alabama',
					'Alaska' => 'Alaska',
					'Arizona' => 'Arizona',
					'Arkansas' => 'Arkansas',
					'California' => 'California',
					'Colorado' => 'Colorado',
					'Connecticut' => 'Connecticut',
					'Delaware' => 'Delaware',
					'Florida' => 'Florida',
					'Georgia' => 'Georgia',
					'Hawaii' => 'Hawaii',
					'Idaho' => 'Idaho',
					'Illinois' => 'Illinois',
					'Indiana' => 'Indiana',
					'Iowa' => 'Iowa',
					'Kansas' => 'Kansas',
					'Kentucky' => 'Kentucky',
					'Louisiana' => 'Louisiana',
					'Maine' => 'Maine',
					'Maryland' => 'Maryland',
					'Massachusetts' => 'Massachusetts',
					'Michigan' => 'Michigan',
					'Minnesota' => 'Minnesota',
					'Mississippi' => 'Mississippi',
					'Missouri' => 'Missouri',
					'Montana' => 'Montana',
					'Nebraska' => 'Nebraska',
					'Nevada' => 'Nevada',
					'New Hampshire' => 'New Hampshire',
					'New Jersey' => 'New Jersey',
					'New Mexico' => 'New Mexico',
					'New York' => 'New York',
					'North Carolina' => 'North Carolina',
					'North Dakota' => 'North Dakota',
					'Ohio' => 'Ohio',
					'Oklahoma' => 'Oklahoma',
					'Oregon' => 'Oregon',
					'Pennsylvania' => 'Pennsylvania',
					'Rhode Island' => 'Rhode Island',
					'South Carolina' => 'South Carolina',
					'South Dakota' => 'South Dakota',
					'Tennessee' => 'Tennessee',
					'Texas' => 'Texas',
					'Utah' => 'Utah',
					'Vermont' => 'Vermont',
					'Virginia' => 'Virginia',
					'Washington' => 'Washington',
					'West Virginia' => 'West Virginia',
					'Wisconsin' => 'Wisconsin',
					'Wyoming' => 'Wyoming',
				),
				'default_value' => '',
			),
			array (
				'key' => 'field_51924df7fd4a8',
				'label' => 'Zipcode',
				'name' => 'zipcode',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_51924e03fd4a9',
				'label' => 'Phone Number',
				'name' => 'phone_number',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_51924e21fd4aa',
				'label' => 'Notes',
				'name' => 'notes',
				'type' => 'textarea',
				'instructions' => 'Enter notes about the course, example: Public; Walking only (caddies / pull carts available); Driving Range Available',
				'default_value' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_51924e53fd4ab',
				'label' => 'Architect',
				'name' => 'architect',
				'type' => 'text',
				'default_value' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_51924e62fd4ac',
				'label' => 'Year',
				'name' => 'year',
				'type' => 'number',
				'instructions' => 'Enter year established',
				'default_value' => '',
			),
			array (
				'key' => 'field_51924e78fd4ad',
				'label' => 'Rate',
				'name' => 'rate',
				'type' => 'text',
				'instructions' => 'Enter the rate or range of rates for this course. Example $75-235',
				'default_value' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_51924f4c3ade4',
				'label' => 'Course Yardage',
				'name' => 'course_yardage',
				'type' => 'textarea',
				'default_value' => '',
				'formatting' => 'br',
			),
			array (
				'key' => 'field_5192503b3ade5',
				'label' => 'Scorecard Image',
				'name' => 'scorecard_image',
				'type' => 'file',
				'save_format' => 'url',
			),
			array (
				'key' => 'field_5192505f3ade6',
				'label' => 'Scorecard Preview',
				'name' => 'scorecard_preview',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
			),
			array (
				'key' => 'field_519250853ade7',
				'label' => 'Course Description',
				'name' => 'course_description',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'course-profiles',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				5 => 'revisions',
				6 => 'author',
				7 => 'categories',
				8 => 'tags',
				9 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_destinations',
		'title' => 'Destinations',
		'fields' => array (
			array (
				'key' => 'field_5192abb4ef231',
				'label' => 'Destination Description',
				'name' => 'destination_description',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_5192aac82c824',
				'label' => 'Homepage Slider',
				'name' => 'homepage_slider',
				'type' => 'true_false',
				'message' => 'Show this destination in the slider on the homepage?',
				'default_value' => 1,
			),
			array (
				'key' => 'field_5192aa742c822',
				'label' => 'Slider Image',
				'name' => 'slider_image',
				'type' => 'image',
				'instructions' => 'RGB .jpg 980px wide, 470px tall',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5192aac82c824',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
			),
			array (
				'key' => 'field_5192aaa82c823',
				'label' => 'Slider Thumbnail',
				'name' => 'slider_thumbnail',
				'type' => 'image',
				'instructions' => 'RGB .jpg 123px wide, 90px tall',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5192aac82c824',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
			),
			array (
				'key' => 'field_51926cdd3d327',
				'label' => 'Destination Map',
				'name' => 'destination_map',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'destinations',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
				0 => 'the_content',
				1 => 'excerpt',
				2 => 'custom_fields',
				3 => 'discussion',
				4 => 'comments',
				5 => 'revisions',
				6 => 'slug',
				7 => 'author',
				8 => 'format',
				9 => 'featured_image',
				10 => 'categories',
				11 => 'tags',
				12 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_feature-post',
		'title' => 'Feature Post',
		'fields' => array (
			array (
				'key' => 'field_5193ca53f25a8',
				'label' => 'Feature Post',
				'name' => 'feature_post',
				'type' => 'true_false',
				'message' => 'Feature this post on the homepage',
				'default_value' => 0,
			),
			array (
				'key' => 'field_5193cb7a0f127',
				'label' => 'Homepage Headline',
				'name' => 'homepage_headline',
				'type' => 'true_false',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_5193ca53f25a8',
							'operator' => '==',
							'value' => '1',
						),
					),
					'allorany' => 'all',
				),
				'message' => 'Make this the headline post on the home page',
				'default_value' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'side',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_site-options',
		'title' => 'Site Options',
		'fields' => array (
			array (
				'key' => 'field_51964dff28ebf',
				'label' => 'Left Block',
				'name' => 'left_block',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_51964e4828ec0',
				'label' => 'Right Block',
				'name' => 'right_block',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
*/