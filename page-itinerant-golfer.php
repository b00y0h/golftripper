<?php
/**
 * Page Template: Itinerant Golfer
 *
 * @package Golf Tripster
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
          <div class="page-inner-content  inset-border triangle">
            <div class="border">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>
				   			 
				
				<?php 

        if(get_field('top_100')) : ?>
        
        <h3 class="top-courses-played-title">Top 100 Courses Played</h3>
        <div class="gs-to-2 clearfix">

  			  <div class="gs ig-course-column left-column">
  			    
        <?php
          $top_100_size = count(get_field('top_100'));
          $i = 1;
          $col_size = ceil(count(get_field('top_100'))/2) ;
        	while(has_sub_field('top_100')) : 
        	  $column = ($i >= $col_size) ? 2 : 1;
        	  
        	  
        	  if((count($top_100_size) % 2 != 0)){
        	    if($i % 2 == 0){
        	      $row_label = 'even';
        	    }
        	    else{
        	      $row_label = 'odd';
        	    }
      	    }
      	    else{
      	      if($i % 2 == 0){
        	      $row_label = 'even';
        	    }
        	    else{
        	      $row_label = 'odd';
        	    }
      	      
      	    }
        	  
        	    
        	      
        	  
        	  
        	  ?>
        	  
        	  <div class="ig-course clearfix <?php echo $row_label; ?>">

        	    
        	    <?php if(get_sub_field('played')): ?>
        	      <img src="<?php bloginfo('template_directory'); ?>/images/checkbox-checked.png" align="left">
        	    <?php else: ?>
        	      <img src="<?php bloginfo('template_directory'); ?>/images/checkbox-unchecked.png" align="left">
        	    <?php endif; ?>
        	    
        	    <span class="ig-course-number"><?php echo $i; ?></span>
        	    
        	    <div class="ig-course-description">
        	      <div class="ig-course-name">
          	    <?php if(get_sub_field('course_profile')): ?>
          	      <a href="<?php echo get_permalink(get_sub_field('course_profile')->ID); ?>">
          	    <?php endif; ?>
          	    <?php echo get_sub_field('course_name'); ?>
          	    <?php if(get_sub_field('course_profile')): ?>
          	      </a>
          	    <?php endif; ?>  
          	    </div> 
          	    <?php echo get_sub_field('city_state'); ?>
        	    </div>
        	  </div>
        	  
        	  <?php if($column == 2 && $i==$col_size): ?>
        	  
          	</div>
    				<div class="gs ig-course-column right-column">
        	  
          	<?php endif;  ?>
        	  
        	<?php
        	  $i++;
        	endwhile; ?>
        	
        	</div>
				</div>

        <?php endif; ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() )
						comments_template();
				?>

			<?php endwhile; // end of the loop. ?>
                </div> <!-- .page-inner-content -->
            </div> <!-- .border -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
