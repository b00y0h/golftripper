<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Golf Tripster
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
          <div class="page-inner-content  inset-border">
            <div class="border">


		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title category-title">
					<?php
						printf( __( '%s', 'golftripster' ), '<span>' . single_cat_title( '', false ) . '</span>' );					
					?>
				</h1>
				<?php
					if ( is_category() ) :
						// show an optional category description
						$category_description = category_description();
						if ( ! empty( $category_description ) ) :
							echo apply_filters( 'category_archive_meta', '<div class="taxonomy-description">' . $category_description . '</div>' );
						endif;

					elseif ( is_tag() ) :
						// show an optional tag description
						$tag_description = tag_description();
						if ( ! empty( $tag_description ) ) :
							echo apply_filters( 'tag_archive_meta', '<div class="taxonomy-description">' . $tag_description . '</div>' );
						endif;

					endif;
				?>
			</header><!-- .page-header -->
      <br>
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); 
			
			    $course = get_field('reviewed_course'); 
			?>
        
        <div class="course-review-listing">
          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>            
          	<header class="post-header">          	  
          		<h1 class="entry-title category-post"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
          		<span style="font-size:12px;">Written by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author_meta('display_name'); ?></a></span>
          	</header>
          	
          	<div class="course-review-logo-wrapper center-vertical-wrapper">
            	<a href="<?php echo get_permalink($course->ID); ?>">
                <img class="course-logo-mini center-vertical" src="<?php the_field('course_logo',$course->ID); ?>">
              </a>	  
            </div>
				  
  				  <p>
  				  	<?php echo wp_trim_words($post->post_content, 120, ' '); ?>	<a href="<?php the_permalink(); ?>" class="more-link">More&nbsp;&gt;</a>
  				  </p>
  				  
  				  
        		<?php include('social-snippet.php'); ?>
        		
  				</article>
				</div>

				<hr style="clear:both;">
				
			<?php endwhile; ?>

			<?php golftripster_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>
                </div> <!-- .page-inner-content -->
            </div> <!-- .border -->

		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>