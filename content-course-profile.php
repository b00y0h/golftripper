<?php
/**
 * @package Golf Tripster
 */

 global $post;


 $destination = get_the_terms($post->ID, 'destinations');
 if($destination){
   sort($destination);
   $destination = $destination[0];
 }

?>


    <div class="page-inner-content inset-border triangle overview">
        <script type="text/javascript">
          (function($){
            $(document).ready(function(){

              	$.getJSON(ajaxurl, {action: 'get_course_rating',postID: '<?php echo $post->ID; ?>'}, function(response) {
                  if(response.successMsg=='Success'){
                    for (var key in response.ratings) {
                      if (response.ratings.hasOwnProperty(key)) {
                         //console.log(key + " -> " + response.ratings[key]);
                         $("."+key).rateit('value', response.ratings[key]);
                      }
                    }
                    $('.rateit').rateit('readonly',true);
                  }
                });

                $("#toggle-rating").click(function(){
                  if($('.rateit:first').rateit('readonly')){
                    // go into edit
                    $('.rateit').rateit('readonly',false).addClass('adding-rating');
                    $('.rateit').rateit('value',0);
                    $(this).html('Save Rating');
                  }
                  else{
                    // save rating
                    $.getJSON(ajaxurl, {action: 'get_course_rating',postID: '<?php echo $post->ID; ?>'}, function(response) {
                      if(response.successMsg=='Success'){
                        for (var key in response.ratings) {
                          if (response.ratings.hasOwnProperty(key)) {
                             //console.log(key + " -> " + response.ratings[key]);
                             $("."+key).rateit('value', response.ratings[key]);
                          }
                        }
                        $('.rateit').rateit('readonly',true).removeClass('adding-rating');
                      }
                    });
                    $('.rateit').rateit('readonly',true).removeClass('adding-rating');
                    $(this).html('Add Your Rating');
                    $(".comment-wrapper").show();
                  }

                })

              	$(".rating-wrapper .rateit").bind('rated reset', function(e) {
              	  var ri = $(this);
              	  rate_parameter = ri.data('rateit-backingfld').replace('#','');
              	  rate_value = ri.rateit('value');
              	  data = {
              	    action: 'set_course_rating',
              	    postID: '<?php echo $post->ID; ?>',
              	    parameter: rate_parameter,
              	    rating: rate_value,
              	  };
              	  $.getJSON(ajaxurl, data, function(response) {
                    if(response.successMsg=='Success'){
                      //console.log("thank you");
                    }
                  });

              	})

                $(".comment-toggler").click(function(){
                  $(".comment-wrapper").show();
                })
            })
          })(jQuery);
        </script>

        <div class="page-inner-content overview course-profile-post">
            <div class="border">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header">
                        <h1 class="entry-title hide"><?php the_title(); ?></h1>
                    </header><!-- .entry-header -->
<div class="gs-to-2">

                <div class="gs">
                    <div class="course-logo-full">
                        <img src="<?php the_field('course_logo'); ?>" alt="">
                    </div>


                     <ul class="rankings four-up">
                        <?php if(get_field('golf_digest_public_ranking')): ?>
                          <li class="ranking"><div class="icon-course-golf-digest-public-button"><?php the_field('golf_digest_public_ranking'); ?></div><span class="hide">Golf Digest public ranking: </span></li>
                        <?php endif; ?>
                        <?php if(get_field('golf_magazine_public_ranking')): ?>
                          <li class="ranking"><div class="icon-course-golf-magazine-public-button"><?php the_field('golf_magazine_public_ranking'); ?></div><span class="hide">Golf Magazine public ranking: </span></li>
                        <?php endif; ?>
                        <?php if(get_field('golf_digest_ranking')): ?>
                          <li class="ranking"><div class="icon-course-golf-digest-button"><?php the_field('golf_digest_ranking'); ?></div><span class="hide">Golf Digest ranking: </span></li>
                        <?php endif; ?>
                        <?php if(get_field('golf_magazine_ranking')): ?>
                          <li class="ranking"><div class="icon-course-golf-magazine-button"><?php the_field('golf_magazine_ranking'); ?></div><span class="hide">Golf Magazine ranking: </span></li>
                        <?php endif; ?>
                      </ul>
                </div>


                <div class="gs">

                  <div class="course-profile">
                      <h2><?php if($destination){ echo $destination->name . ' | '; } ?><?php the_title(); ?></h2>
                      <p class="year-architect">
                        <?php if(get_field('architect')) : ?>Architect: <?php the_field('architect'); ?> <br><?php endif; ?>
                        <?php if(get_field('year')) : ?>Year: <?php the_field('year'); ?> <br><?php endif; ?>
                        <?php if(get_field('rate')) : ?>Rate: <?php the_field('rate');  endif; ?>
                      </p>
                      <address>
                        <?php if(get_field('address')){ echo get_field('address'); } ?><br>

                        <?php if(get_field('city')){ echo get_field('city').','; } ?>
                        <?php the_field('state') ?>
                        <?php if(get_field('zipcode')){ echo get_field('zipcode').' '; } ?>
                        <br><span class="tel"><?php echo format_phone_number(get_field('phone_number')); ?></span>
                      </address>
											<p>
												Course Access: <?php echo (get_field('access')=='public') ? 'Public' : 'Private'; ?>
											</p>

                      <p class="italic"><?php the_field('notes'); ?></p>
                  </div>

                </div> <!-- .gs -->


            </div> <!-- .gs-to-2 -->

            <hr class="cb">


                <?php $tees = get_field('course_yardage');
                    if(count($tees) > 0): ?>
                    <table class="course-yardage-table">
                        <thead>
                            <tr>
                                <th>Tee</th>
                                <th>Yardage</th>
                                <th>Par</th>
                                <th>Rating&nbsp;/&nbsp;Slope</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php foreach($tees as $tee): if($tee['tee_name']!= ''): ?>
                            <tr>
                                <td><?php echo $tee['tee_name']; ?></td>
                                <td><?php echo $tee['yardage']; ?></td>
                                <td><?php echo $tee['par']; ?></td>
                                <td><?php echo $tee['rating_slope']; ?></td>
                            </tr>
                          <?php endif; endforeach; ?>
                        </tbody>
                    </table>
                    <p><?php if(get_field('scorecard')): ?><a href="<?php the_field('scorecard'); ?>" class="more-link" target="_blank">View Score Card &gt;</a><?php endif;?></p>
                    <hr>
                  <?php endif; ?>


                <div class="entry-content course-description">
                        <?php the_field('course_description'); ?>
                </div> <!-- .entry-content .course-description -->

                <?php include('social-snippet.php'); ?>

                <?php edit_post_link(); ?>


                <?php if(get_field('access') == 'public' && false): ?>
                      <div class="rating-wrapper gs-to-2">

                        <div class="gs">

                          <div class="rating-header">
                            <div class="rating-title"><strong>Golf Tripper Ratings</strong></div>
                            <div class="rating-description">1 = Unsatisfactory  5 = Excellent</div>
                          </div>
                          <div class="rating-parameter">
                            <div class="rating-parameter-name">Service and Amenities</div>
                            <select id="service_amenities_parameter">
                                <option value="0"></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <div class="rateit service_amenities_parameter" data-rateit-backingfld="#service_amenities_parameter" data-rateit-resetable="false" ></div>
                          </div>
                          <div class="rating-parameter">
                            <div class="rating-parameter-name">Maintenance</div>
                            <select id="maintenance_parameter">
                                <option value="0"></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <div class="rateit maintenance_parameter" data-rateit-backingfld="#maintenance_parameter" data-rateit-resetable="false" ></div>
                          </div>
                          <div class="rating-parameter">
                            <div class="rating-parameter-name">Scenery</div>
                            <select id="scenery_parameter">
                                <option value="0"></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <div class="rateit scenery_parameter" data-rateit-backingfld="#scenery_parameter" data-rateit-resetable="false" >
                            </div>
                          </div>

                        </div>

                        <div class="gs">
                          <div class="rating-shim"></div>


                          <div class="rating-parameter">
                            <div class="rating-parameter-name">Value</div>
                            <select id="value_parameter">
                                <option value="0"></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <div class="rateit value_parameter" data-rateit-backingfld="#value_parameter" data-rateit-resetable="false" >
                            </div>
                          </div>
                          <div class="rating-parameter">
                            <div class="rating-parameter-name">Difficulty</div>
                            <select id="difficulty_parameter">
                                <option value="0"></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <div class="rateit difficulty_parameter" data-rateit-backingfld="#difficulty_parameter" data-rateit-resetable="false" >
                            </div>
                          </div>
                          <div class="rating-parameter">
                            <div class="rating-parameter-name">Fast Pace of Play</div>
                            <select id="pace_play_parameter">
                                <option value="0"></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <div class="rateit pace_play_parameter" data-rateit-backingfld="#pace_play_parameter" data-rateit-resetable="false" >
                            </div>
                          </div>


                        </div>
                        <div class="clear">
                          <div id="toggle-rating">
                              Add Your Rating
                          </div>
                        </div>
                      </div>



                        <br class="cb">

                <?php endif; ?>


            </article>



            </div> <!-- .border -->
        </div> <!-- .page-inner-content overview course-profile-post -->
</div>
        <?php
          $takes = get_posts(array(
            'numberposts' => 2,
            'post_type' => 'post',
            'meta_key' => 'reviewed_course',
            'meta_value' => $post->ID,
            'orderby' => 'post_date',
            'order' => 'desc'
          ));
        $count = count($takes);
        $word_count = ($count == 1) ? 250 : 150;
        if($count > 0 ):  ?>
        <div class="page-inner-content  inset-border triangle get-their-take">
            <div class="border gs-to-1 cf">
                <h2>Read more about the <?php the_title(); ?> Experience</h2>


                <?php foreach($takes as $take): ?>

                  <div class="media gs">

	                  <div class="pull-left">

										  <a href="<?php echo get_author_posts_url($take->post_author); ?>">
                        <?php echo get_avatar($take->post_author, 60); ?>
                      </a>

                      <div class="author-social gs-to-<?php echo (get_the_author_meta('facebook', $take->post_author) && get_the_author_meta('twitter', $take->post_author)) ? 3 : 2; ?>">

                        <?php if(get_the_author_meta('facebook', $take->post_author)): ?>
                        <a href="<?php echo get_the_author_meta('facebook', $take->post_author); ?>" class="gs" align="center" target="_blank">
                          <img src="<?php bloginfo('template_directory'); ?>/images/author-fb.png" width="18">
                        </a>
                        <?php endif; ?>
                        <?php if(get_the_author_meta('twitter', $take->post_author)): ?>
                        <a href="https://twitter.com/<?php echo get_the_author_meta('twitter', $take->post_author); ?>" class="gs" align="center" target="_blank">
                          <img src="<?php bloginfo('template_directory'); ?>/images/author-tw.png" width="18">
                        </a>
                        <?php endif; ?>
                        <a href="/contact" class="gs" align="center" target="_blank">
                          <img src="<?php bloginfo('template_directory'); ?>/images/author-em.png" width="18">
                        </a>

                      </div>
                    </div>

                    <div class="media-body">

                      <a href="<?php echo get_author_posts_url($take->post_author); ?>">
                        <h4 class="media-heading"><?php echo get_the_author_meta('display_name', $take->post_author); ?></h4>
                      </a>
                      
                      <a href="<?php echo get_permalink($take->ID); ?>"><h3><?php echo $take->post_title; ?></h3></a>
                      <?php echo wpautop(wp_trim_words($take->post_content, $word_count) . ' <a href="' . get_permalink($take->ID) . '" class="more-link">More&nbsp;&gt;</a>'); ?>
                    </div>
                  </div>

                <?php endforeach; ?>

            </div> <!-- .border -->
        </div> <!-- .inset-border .triangle -->
        <?php endif;  ?>

        <div class="page-inner-content  inset-border triangle">
            <div class="border">
         <?php
                // If comments are open or we have at least one comment, load up the comment template
                if ( comments_open() || '0' != get_comments_number() )
                    comments_template();
            ?>
            </div>
        </div>