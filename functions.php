<?php
/**
 * Golf Tripster functions and definitions
 *
 * @package Golf Tripster
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

/*
 * Load Jetpack compatibility file.
 */
require( get_template_directory() . '/inc/jetpack.php' );

//disable WordPress sanitization to allow more than just $allowedtags from /wp-includes/kses.php
remove_filter('pre_user_description', 'wp_filter_kses');
//add sanitization for WordPress posts
add_filter( 'pre_user_description', 'wp_filter_post_kses');

if ( ! function_exists( 'golftripster_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function golftripster_setup() {

	/**
	 * Custom template tags for this theme.
	 */
	require( get_template_directory() . '/inc/template-tags.php' );

	/**
	 * Custom functions that act independently of the theme templates
	 */
	require( get_template_directory() . '/inc/extras.php' );

	/**
	 * Customizer additions
	 */
	require( get_template_directory() . '/inc/customizer.php' );

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on Golf Tripster, use a find and replace
	 * to change 'golftripster' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'golftripster', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'golftripster' ),
        'footer_menu' => __( 'Footer Menu', 'golftripster' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );
}
endif; // golftripster_setup
add_action( 'after_setup_theme', 'golftripster_setup' );


/**
 * Setup the WordPress core custom background feature.
 *
 * Use add_theme_support to register support for WordPress 3.4+
 * as well as provide backward compatibility for WordPress 3.3
 * using feature detection of wp_get_theme() which was introduced
 * in WordPress 3.4.
 *
 * @todo Remove the 3.3 support when WordPress 3.6 is released.
 *
 * Hooks into the after_setup_theme action.
 */
function golftripster_register_custom_background() {
	$args = array(
		'default-color' => 'ffffff',
		'default-image' => '',
	);

	$args = apply_filters( 'golftripster_custom_background_args', $args );

	if ( function_exists( 'wp_get_theme' ) ) {
		add_theme_support( 'custom-background', $args );
	} else {
		define( 'BACKGROUND_COLOR', $args['default-color'] );
		if ( ! empty( $args['default-image'] ) )
			define( 'BACKGROUND_IMAGE', $args['default-image'] );
		add_custom_background();
	}
}
add_action( 'after_setup_theme', 'golftripster_register_custom_background' );

function sortArrayByArray($array,$orderArray) {
    $ordered = array();
    foreach($orderArray as $key) {
    	if(array_key_exists($key,$array)) {
    		$ordered[$key] = $array[$key];
    		unset($array[$key]);
    	}
    }
    return $ordered + $array;
}

/**
 * Register widgetized area and update sidebar with default widgets
 */
function golftripster_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'golftripster' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'golftripster_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function golftripster_scripts() {
    // wp_enqueue_script( 'Golf-Tripster-navigation', get_template_directory_uri() . '/js/navigation-ck.js', array(), '20120206', true );

    // wp_enqueue_script( 'Golf-Tripster-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix-ck.js', array(), '20130115', true );

    // wp_enqueue_script( 'rateit', get_template_directory_uri() . '/js/jquery.rateit-ck.js', array('jquery'), '20130521', true );

    // wp_enqueue_script( 'nivo', get_template_directory_uri() . '/js/jquery.nivo.slider.pack.js', array( 'jquery', 'application' ), '2013', true );

    // wp_enqueue_script( 'orangebox', get_template_directory_uri() . '/js/orangebox.min.js', array( 'jquery' ), '20130514', true );

    wp_enqueue_script( 'application', get_template_directory_uri() . '/js/application-ck.js', array('jquery'), '4545', true );

    wp_enqueue_style( 'Golf-Tripster-style', get_stylesheet_uri() );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    if ( is_singular() && wp_attachment_is_image() ) {
        wp_enqueue_script( 'Golf Tripster-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation-ck.js', array( 'jquery' ), '20120202' );
	}
}
add_action( 'wp_enqueue_scripts', 'golftripster_scripts' );

/**
 * Implement the Custom Header feature
 */
//require( get_template_directory() . '/inc/custom-header.php' );

//define( 'ACF_LITE', true );
if ($_SERVER['SERVER_NAME'] !== "golftripper.dev") {
  include_once(get_template_directory() . '/advanced-custom-fields/acf.php');
  include_once(get_template_directory() . '/inc/acf.php');
}
require_once (get_template_directory() . '/inc/theme-menu-walker.php');    // Custom Menu Walker


function format_phone_number($str){
  $num = preg_replace('/[^0-9]/', '', $str);

  $len = strlen($num);
  if($len == 7)
  $num = preg_replace('/([0-9]{3})([0-9]{4})/', '$1-$2', $num);
  elseif($len == 10)
  $num = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $num);

  return $num;
}

add_filter('single_template', "handle_single_category_templates");
function handle_single_category_templates($single_template){
  global $post;
  foreach( (array) get_the_category() as $cat ) {
     if ( file_exists(dirname( __FILE__ ) . "/single-cat-{$cat->slug}.php") ) {
       return dirname( __FILE__ ). "/single-cat-{$cat->slug}.php";
     }
  }
  return $single_template;

}

function add_ajax_library() {
    $html = '<script type="text/javascript">';
        $html .= 'var ajaxurl = "' . admin_url( 'admin-ajax.php' ) . '"';
    $html .= '</script>';
    echo $html;

}
add_action('wp_head', 'add_ajax_library');



function edit_course_columns( $columns ) {
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Course' ),
		'destination' => __( 'Destination' ),
		'date' => __( 'Date' )
	);
	return $columns;
}
add_filter( 'manage_edit-course-profiles_columns', 'edit_course_columns' ) ;


function manage_course_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {
		case 'destination' :

			/* Get the genres for the post. */
			$terms = get_the_terms( $post_id, 'destinations' );

			/* If terms were found. */
			if ( !empty( $terms ) ) {

				$out = array();

				/* Loop through each term, linking to the 'edit posts' page for the specific term. */
				foreach ( $terms as $term ) {
					$out[] = sprintf( '<a href="%s">%s</a>',
						esc_url( add_query_arg( array( 'action'=>'edit', 'taxonomy' => 'destinations', 'tag_ID' => $term->term_id, 'post_type' => $post->post_type), 'edit-tags.php' ) ),
						esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'destinations', 'display' ) )
					);
				}

				/* Join the terms, separating them with a comma. */
				echo join( ', ', $out );
			}

			/* If no terms were found, output a default message. */
			else {
				_e( 'No Destination' );
			}

			break;

		/* Just break out of the switch statement for everything else. */
		default :
			break;
	}
}
add_action( 'manage_course-profiles_posts_custom_column', 'manage_course_columns', 10, 2 );


function clean_destination_meta(){
    global $current_screen;
    if( $current_screen->id == 'edit-destinations') {  ?>
      <script type="text/javascript">
      jQuery(document).ready( function($) {
          <?php if(isset($_GET['tag_ID'])): ?>
          $('#description, #parent').parents('tr.form-field').remove();
          <?php else: ?>
          $('#tag-description, #parent').parents('div.form-field').remove();
          <?php endif; ?>
      });
      </script>
      <?php
    }
}
add_action( 'admin_head', 'clean_destination_meta' );

function get_top100_rankings($limit=5, $id=null){
	if($id == null){
		global $post;
		$id = $post->ID;
	}
	$raw_rankings = get_field('full_rankings', $id);

	if($raw_rankings){
		$rankings = preg_split('/[\n\r]+/', $raw_rankings);
		$i = 1;
		foreach($rankings as $key => $rank){
		  if($i>$limit){ break; }
			list($course, $info) = explode(',', $rank, 2);
			$output[$key+1] = array(
				"course" => $course,
				"info" => $info
			);
			$i++;
		}

	}

	return $output;
}

function get_courses_by_state(){
   global $wpdb;
   $courses = false;
   $states = $wpdb->get_col("SELECT DISTINCT meta_value
    	FROM $wpdb->postmeta WHERE meta_key = 'state' ORDER BY meta_value ASC" );

    foreach($states as $state){
      $courses[$state] = $wpdb->get_col("SELECT posts.ID
      	FROM $wpdb->postmeta postmeta, $wpdb->posts posts
      	WHERE postmeta.post_id = posts.ID
      	  AND postmeta.meta_key = 'state'
      	  AND postmeta.meta_value='$state'
      	  AND posts.post_type = 'course-profiles'
      	  AND posts.post_status = 'publish'
					ORDER BY posts.post_name
      	" );
      if(count($courses[$state]) < 1){
        unset($courses[$state]);
      }
    }

    return $courses;
}

function get_course_by_ranking($list=false){
  if(!$list){return false;}

  switch($list){
    case 'golf-digest-top-100':
      $list = 'golf_digest_ranking';
    break;
    case 'golf-digest-top-100-public':
      $list = 'golf_digest_public_ranking';
    break;
    case 'golf-magazine-top-100':
      $list = 'golf_magazine_ranking';
    break;
    case 'golf-magazine-top-100-public':
      $list = 'golf_magazine_public_ranking';
    break;
  }

  global $wpdb;
  $courses = $wpdb->get_results("SELECT postmeta.meta_value as rank, posts.ID
  	FROM $wpdb->postmeta postmeta, $wpdb->posts posts
  	WHERE postmeta.post_id = posts.ID
  	  AND postmeta.meta_key = '$list'
  	  AND postmeta.meta_value !=''
  	  AND posts.post_type = 'course-profiles'
  	  AND posts.post_status = 'publish'
  	" );
  foreach($courses as $course){
    $output[$course->ID] = $course->rank;
  }


  return $output;
}

function get_random_top100(){
  global $wpdb;
  $output = false;
  $top_100 = $wpdb->get_results("SELECT ID
  	FROM $wpdb->posts posts
  	WHERE posts.post_type = 'top-100'
  	  AND posts.post_status = 'publish'
  	ORDER BY RAND()
  	LIMIT 1
  ");
  if($top_100){
    $output = get_post($top_100[0]->ID);
  }

  return $output;
}

function modify_contact_methods($profile_fields) {

	// Add new fields
	$profile_fields['twitter'] = 'Twitter Username';
	$profile_fields['facebook'] = 'Facebook URL';

	// Remove old fields
	unset($profile_fields['aim']);
	unset($profile_fields['yim']);
  unset($profile_fields['jabber']);

	return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods');
