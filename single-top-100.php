<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Golf Tripster
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
          <div class="page-inner-content  inset-border triangle">
            <div class="border">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part('content', 'top-100'); ?>
			
		<?php endwhile; // end of the loop. ?>
                </div> <!-- .page-inner-content -->
            </div> <!-- .border -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>