<?php
/**
 * The template for displaying a single destination.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Golf Tripster
 */

get_header();
$destination = get_term_by('slug',get_query_var('destinations'),$taxonomy);
?>

  <div class="slider-wrapper destination-slider">
      <div class="slider center site-main slider-wrapper">
          <div id="destination-slider" class="nivoSlider">
            <?php
            $sibling_courses = get_posts(array(
                'post_type' => 'course-profiles',
                'destinations' => $destination->slug,
                'meta_key' => 'scenic_image',
                'orderby' => 'menu_order',
                'order' => 'ASC' )
            );
            if($sibling_courses): foreach($sibling_courses as $course):
              setup_postdata($course); ?>
              <img src="<?php the_field('scenic_image', $course->ID); ?>" alt="<?php the_title(); ?>" width="100%">
            <?php endforeach; endif; ?>
          </div>
      </div>

      <div class="slider-title">
        <?php echo $destination->name; ?>
      </div>
  </div>

	<div id="primary" class="content-area">
		<div id="content" class="site-content destination-content" role="main">

      <div class="destination-inner-content  inset-border triangle">
        <div class="border cf">

          <h1 class="destination-name"><?php echo $destination->name; ?></h1>

          <div class="destination-description">
            <?php the_field('destination_description', 'destinations_' . $destination->term_id); ?>
          </div>

          <div class="destination-courses-wrapper">

            <?php
            query_posts(array(
                'post_type' => 'course-profiles',
                'showposts' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'destinations',
                        'terms' => $destination->term_id,
                        'field' => 'term_id',
                    )
                ),
                'orderby' => 'menu_order',
                'order' => 'ASC' )
            );
            if (have_posts()) : while (have_posts()) : the_post(); ?>

            <div class="destination-course cf center-vertical-wrapper">
              <div class="gs gs-1-3 " align="center">
                <a href="<?php the_permalink(); ?>">
                  <?php if(get_field('course_logo')): ?>
                    <img src="<?php the_field('course_logo'); ?>" height="170" class="center-vertical">
                  <?php else: ?>
                    <img src="http://dummyimage.com/170x170/fff/fff&amp;text=<?php the_title(); ?> Logo">
                  <?php endif; ?>
                </a>
              </div>
              <div class="gs gs-2-3 destination-scenic-image">
                <a href="<?php the_permalink(); ?>">
                  <?php if(get_field('scenic_image')): ?>
                    <img src="<?php the_field('scenic_image'); ?>" width="475">
                  <?php else: ?>
                    <img src="http://dummyimage.com/475x166/fff/fff">
                  <?php endif; ?>
                </a>
              </div>
            </div>

            <?php endwhile;
            else: ?>

              This destination does not have any courses yet. Please check back soon!

            <?php endif; ?>


          </div>


        </div>
			</div><!-- .destination-content -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
