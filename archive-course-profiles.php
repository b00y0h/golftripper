<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Golf Tripster
 */

get_header();

global $post;
?>
	<section id="primary" class="content-area gs gs-3-4">
		<div id="content" class="site-content" role="main">
          <div class="page-inner-content  inset-border">
            <div class="border">
                <header class="page-header">
          				<h1 class="course-profile-archive-title">
          				  Course Profiles by State
          				</h1>

          				<div class="course-profiles-archive-intro">
          				  <?php the_field('course_profiles_intro', 'options'); ?>
          				</div>

          			</header>


          			<div class="profile-wrapper">

          			  <?php
          			   $courses =  get_courses_by_state();
                   foreach($courses as $state=>$course_ids): ?>
          			    <div class="state-wrapper cf">
            			    <div class="state-label">
            			      <h3><?php echo $state; ?></h3>
            			    </div>

          			      <div class="state-courses">

          			        <?php foreach ($course_ids as $course_id):
          			          $post = get_post($course_id);
          			          setup_postdata($post);
          			        ?>
          			        <div class="state-course center-vertical-wrapper">
          			          <a href="<?php the_permalink(); ?>">
          			            <?php if(get_field('golf_digest_public_ranking')||get_field('golf_magazine_public_ranking')||get_field('golf_digest_ranking')||get_field('golf_magazine_ranking')): ?>
            			          <img class="top-100-sash" src="<?php bloginfo('template_directory'); ?>/images/top-100-sash.png">
            			          <?php endif; ?>
            			          <?php if(get_field('access') == 'private'): ?>
            			          <div class="private"></div>
            			          <?php endif; ?>
            			          <img class='course-logo center-vertical' src="<?php the_field('course_logo'); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
          			          </a>
          			        </div>

          			        <?php endforeach ?>
          			      </div>
          			    </div>


          			  <?php endforeach; ?>

          			</div>



                </div> <!-- .page-inner-content -->
            </div> <!-- .border -->

		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>