<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Golf Tripster
 */

get_header();
global $wp_query;
 ?>

	<section id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
          <div class="page-inner-content  inset-border">
            <div class="border">

                <div class="gs-to-2 cf">
                  <?php query_posts('post_type=top-100&status=publish&orderby=menu_order&order=ASC'); ?>
              		<?php $i=1; if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>
                    <div class="gs top-100-wrapper <?php echo ($i % 2 == 0) ? 'top-100-right' : 'top-100-left'; ?>">
              				<header class="page-header top-100-header">
              				  <div class="top-100-list-logo">
              				    <a href="<?php the_permalink(); ?>">
              				      <img src="<?php the_field('mini_logo'); ?>">
              				    </a>
              				  </div>
                				<h1 class="top-100-title top-100-archive-title">
                					<a href="<?php the_permalink(); ?>">
                					  <?php the_title(); ?>
                					</a>
                				</h1>	
                				<span class="top-100-date">Last updated on <?php echo get_the_date('F jS, Y'); ?></span>			
                			</header><!-- .page-header -->
  			
                			<ol class="top100-list cf">
                  		   <?php
                  		    $course_rankings = get_course_by_ranking($post->post_name);
                          $rankings = get_top100_rankings(10);
                          $count=1;
                          foreach($rankings as $rank=>$data): 
                            $reviewed_course = array_search($rank, $course_rankings);
                            ?>
                          <li class="<?php echo ($count % 2 == 0) ? 'top-100-even' : 'top-100-odd'; ?>">
                            <?php if($reviewed_course): ?>
                            <a href="<?php echo get_permalink($reviewed_course); ?>">
                              <strong><?php echo $data['course']; ?></strong>
                            </a><br>
                            <?php else: ?>
                            <strong><?php echo $data['course']; ?></strong><br>
                            <?php endif; ?>
                            <span class="subtext"><?php echo $data['info']; ?></span>
                          </li>
                          <?php $count++; endforeach; ?>
                  		</ol>
                  		
                  		<a href="<?php the_permalink(); ?>" class="more-link">More &gt;</a>
                    </div>

										<?php if (($i % 2 == 0 && $i>1)): ?>
											</div>
											<div class="gs-to-2 cf">
												
										<?php endif ?>
										
              		<?php $i++; endwhile; ?>
    

              		<?php else : ?>

              			<?php get_template_part( 'no-results', 'archive' ); ?>

              		<?php endif; ?>
              	</div>
              		
            </div> <!-- .border -->
          </div> <!-- .page-inner-content -->

		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>