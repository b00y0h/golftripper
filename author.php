<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Golf Tripster
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
          <div class="page-inner-content  inset-border">
            <div class="border">


		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title category-title">
					<?php
							/* Queue the first post, that way we know
							 * what author we're dealing with (if that is the case).
							*/
							the_post();
							printf( __( 'Author Profile: %s', 'golftripster' ), '<span class="vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a></span>' );
							/* Since we called the_post() above, we need to
							 * rewind the loop back to the beginning that way
							 * we can run the loop properly, in full.
							 */
							rewind_posts();

					?>
				</h1>

			</header><!-- .page-header -->
			
			<div class="author-bio media gs">
        
        <div class="pull-left">
          

          <?php echo get_avatar(get_the_author_meta( 'ID' ), 117); ?>

                                
          <div class="author-social gs-to-<?php echo (get_the_author_meta('facebook', get_the_author_meta( 'ID' )) && get_the_author_meta('twitter', get_the_author_meta( 'ID' ))) ? 3 : 2; ?>">
            
            <?php if(get_the_author_meta('facebook', get_the_author_meta( 'ID' ))): ?>
            <a href="<?php echo get_the_author_meta('facebook', $take->post_author); ?>" class="gs" align="center" target="_blank">
              <img src="<?php bloginfo('template_directory'); ?>/images/author-fb.png">
            </a>
            <?php endif; ?>
            <?php if(get_the_author_meta('twitter', get_the_author_meta( 'ID' ))): ?>
            <a href="https://twitter.com/<?php echo get_the_author_meta('twitter', get_the_author_meta( 'ID' )); ?>" class="gs" align="center" target="_blank">
              <img src="<?php bloginfo('template_directory'); ?>/images/author-tw.png">
            </a>
            <?php endif; ?>
            <a href="/contact" class="gs" align="center" target="_blank">
              <img src="<?php bloginfo('template_directory'); ?>/images/author-em.png">
            </a>
            
          </div>
        </div>
        
        <div class="media-body">
					
          <h3 class="media-heading"><?php echo get_the_author_meta('display_name', get_the_author_meta( 'ID' )); ?></h3>
          <div class="author-description">
            <?php echo wpautop(get_the_author_meta('description', get_the_author_meta( 'ID' ))); ?>
          </div>
        </div>
      </div>
      

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to overload this in a child theme then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', 'category');
				?>

			<?php endwhile; ?>

			<?php golftripster_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>
                </div> <!-- .page-inner-content -->
            </div> <!-- .border -->

		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>