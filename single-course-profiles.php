<?php
/**
 * The Template for displaying all course profiles.
 *
 * @package Golf Tripster
 */

get_header();
global $post;
?>
    
    <?php while ( have_posts() ) : the_post(); ?>

    <div class="course-header">

      <div class="course-banner">
        <img src="<?php the_field('scenic_image'); ?>" class="course-banner-image" alt="<?php the_title(); ?>">
        <div class="course-banner-text">

          <div class="course-banner-title <?php echo (strlen($post->post_title) > 28) ? ' long-banner-title ' : ''; ?>">
            <?php the_title(); ?>
          </div>
           <div class="course-banner-tagline">
             <?php the_field('banner_tagline'); ?>
           </div>

        </div>
      </div>
      
      <?php
      $destination = get_the_terms($post->ID, 'destinations');
      if($destination) :
        sort($destination);
        $destination = $destination[0];      
      ?>
      <div class="destination-location">
          <h2 class="border-top border-bottom">Destination: <?php echo $destination->name; ?> &gt;</h2>
      </div>
      <?php endif; ?>

      <div class="course-sibling-menu">
        <ul class="clearfix">
          <?php       
          if($destination){
            $sibling_courses = get_posts(array(
                'post_type' => 'course-profiles',
                'destinations' => $destination->slug,
                'orderby' => 'menu_order',
                'order' => 'ASC' )
            );
          }
          
          if($destination && $sibling_courses): foreach($sibling_courses as $course):

          ?>
            <li>
              <a class="<?php echo ($course->ID == $post->ID) ? 'current-sibling-course' : ''; ?>" href="<?php echo get_permalink($course->ID); ?>"><?php echo $course->post_title; ?></a>
            </li>
          <?php endforeach; endif; ?>
        </ul>
      </div>



    </div>





    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">

            <?php get_template_part( 'content', 'course-profile' ); ?>
            
            <div class="clearfix" style="height:15px;"></div>
           


        </div><!-- #content -->
    </div><!-- #primary -->

   <?php endwhile; // end of the loop. ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>