<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Golf Tripster
 */
?>
    </div> <!-- .inside -->

	</div><!-- #main -->
<div class="footer-wrapper cf">
	<footer id="colophon" class="site-footer center" role="contentinfo">

	    <div class="gs-to-3 footer-cols">

	      <div class="gs gs-to-2 distinations-and-profiles">

	        <div class="gs footer-destinations">
	          <span class="footer-title"><a>Golf Destinations</a></span>
	          <ul>
	          <?php
	            $destinations = get_terms('destinations', array('hide_empty'=>false));
	            foreach($destinations as $destination):
	              $destination_url = get_term_link($destination->slug, 'destinations');
	            ?>
	            <li>
	              <a href="<?php echo $destination_url; ?>"><?php echo $destination->name; ?></a>
	            </li>
	          <?php endforeach;?>
	          </ul>
	        </div>
	        <div class="gs">
	          <span class="footer-title"><a href="/course-profiles/">Course Profiles</a></span>
	          <ul>
	            <?php
  	            query_posts(array(
                    'post_type' => 'course-profiles',
                    'posts_per_page' => '5',
                    'post_status' => 'publish',
                    'orderby' => 'ID',
                    'order' => 'DESC' )
                );
                if (have_posts()) : while (have_posts()) : the_post(); ?>
  	            <li>
  	              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
  	            </li>
  	          <?php endwhile; endif;?>
  	            <li>
  	              <a href="/course-profiles/">More...</a>
  	            </li>
	          </ul>
	        </div>

	      </div>

	      <div class="gs gs-to-2 golf-life-and-top-100">

	        <div class="gs">
	          <span class="footer-title"><a href="#">Golf Life</a></span>
	          <ul>
	            <?php
	              $args=array(
	              'orderby' => 'none',
                'include' => array(6,8,4,7,5,3),
                );
              $categories=get_terms('category', $args);
              foreach($categories as $category){
                $cat_list[$category->term_id] = $category;
              }
              $cat_list = sortArrayByArray($cat_list, array(6,8,4,7,5,3));

              foreach($cat_list as $category) : ?>
                  <li>
                  <a href="<?php echo get_category_link( $category->term_id ); ?>"><?php  echo $category->name;?></a>
                  </li>
              <?php endforeach; ?>
	          </ul>
	        </div>
	        <div class="gs">
	          <span class="footer-title"><a href="#">Top 100 Rankings</a></span>
             <ul>
  	            <?php
    	            query_posts(array(
                      'post_type' => 'top-100',
                      'post_status' => 'publish',
                      'orderby' => 'ID',
                      'order' => 'ASC' )
                  );
                  if (have_posts()) : while (have_posts()) : the_post(); ?>
    	            <li>
    	              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    	            </li>
    	          <?php endwhile; endif;?>
  	          </ul>

            <br>
	          <span class="footer-title"><a href="/itinerant-golfer/">Itinerant Golfer</a></span>
	        </div>

	      </div>

	      <div class="gs contact widget_stay_connected ">

	        <span class="footer-title"><a>Contact</a></span>
	        <p>
	          <a href="/contact">Send us an email</a>
	        </p>

	        <br>
					 <span class="footer-title"><a>E-Newsletter</a></span>
	         <div class="mc_embed_signup">
              <form action="http://golftripper.us7.list-manage2.com/subscribe/post?u=2181d60de8e80677729359ce7&amp;id=76156ecd72" method="post" name="mc-embedded-subscribe-form" class="validate mc-embedded-subscribe-form" target="_blank" novalidate="">
                <div class="mc-field-group mc-input-outline">
                    <input type="email" onblur="if (this.value == '') {this.value ='enter your email';}" onfocus="if (this.value == 'enter your email') {this.value = '';}" value="enter your email" name="EMAIL" class="required email footer-signup-input mce-EMAIL" required>

                    <input class="mc-embedded-subscribe newslettersubmit footer-newsletter-submit" src="<?php echo get_template_directory_uri(); ?>/images/signup.gif" type="image" alt="Submit">
                </div>
              </form>
              <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
              </div>
            </div>
            <script type="text/javascript" charset="utf-8" src="<?php bloginfo('template_directory'); ?>/js/mailchimp-widget-ck.js"></script>
            
           <br>
           <a href="https://plus.google.com/115267299457063750847" rel="publisher">Google+</a>
	      </div>
        
	    </div>

	  <br class="cb">

    


	</footer><!-- #colophon -->
</div>

<div class="bottom-stripe cf">
  <div class="center site-footer">

    <div class="pull-right footer-social">
        <a href="/feeds/rss" class="icon-rss" target="_blank">RSS</a>
        <a href="https://twitter.com/ItinerantGolfer" class="icon-twitter" target="_blank">Twitter</a>
        <a href="https://www.facebook.com/pages/Golf-Tripper/512678492101292" class="icon-facebook" target="_blank">Facebook</a>        
    </div>
    <div class="footer-byline">
    &copy; <?php echo date('Y'); ?> Golf Tripper |
    <a href="/privacy-policy">Privacy Policy</a> |
    Site designed by <a href="http://www.releasethehounds.tv" target="_blank">Release The Hounds</a>
    </div>


  </div>
</div>

</div><!-- #page -->

<?php wp_footer(); ?>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-41333758-1', 'golftripper.com');
    ga('send', 'pageview');

  </script>

</body>
</html>