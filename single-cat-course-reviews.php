<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Golf Tripster
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
          <div class="page-inner-content  inset-border triangle">
            <div class="border">

		<?php while ( have_posts() ) : the_post(); 
		  $course = get_field('reviewed_course'); 
		  $destination = get_the_terms($course->ID, 'destinations');
		  if($destination){
		    sort($destination);
        $destination = $destination[0];
		  }
      
		?>		  

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      	<header class="entry-header course-review-header">
      	  <div class="author-image"><?php echo get_avatar(get_the_author_meta('ID'), '60'); ?></div>
      		<h1 class="entry-title"><?php the_author(); ?>'s Take on <?php echo $course->post_title; ?></h1>
      		
      		 <hr>

            <div class="course-profile">
                <div class="course-profile-mini-logo-wrapper">
                  <a href="<?php echo get_permalink($course->ID); ?>">
                    <img class="course-logo-mini" src="<?php the_field('course_logo',$course->ID); ?>">
                  </a>
                </div>
                <div class="pull-left course-profile-text">
                  <h2>
                    <?php if($destination): ?>
                    <a href="<?php echo get_term_link($destination); ?>"><?php echo $destination->name; ?></a> 
                    | 
                    <?php endif; ?>
                    <a href="<?php echo get_permalink($course->ID); ?>"><?php echo $course->post_title; ?></a>
                  </h2>
                  <p>
                    <?php if(get_field('architect', $course->ID)): ?>
                    Architect: <?php the_field('architect', $course->ID); ?><br>
                    <?php endif; ?>
                    <?php if(get_field('year', $course->ID)): ?>
                    Year: <?php the_field('year', $course->ID); ?><br>
                    <?php endif; ?>
                    <?php if(get_field('rate', $course->ID)): ?> 
                    Rate: <?php the_field('rate', $course->ID); ?>
                    <?php endif; ?>
                  </p>
                  <address>
                    <?php if(get_field('address', $course->ID)): the_field('address', $course->ID); ?>, <?php endif; ?>
                    <?php if(get_field('city', $course->ID)): the_field('city', $course->ID); ?>, <?php endif; ?> 
                    <?php if(get_field('state', $course->ID)): the_field('state', $course->ID); endif; ?>
                    <?php if(get_field('zipcode', $course->ID)): the_field('zipcode', $course->ID); endif; ?>
                    <?php if(get_field('phone_number', $course->ID)): ?>
                    <br>  
                    <span class="tel"><?php echo format_phone_number(get_field('phone_number', $course->ID)); ?></span>
                    <?php endif; ?>
                  </address>
                  <p class="italic"><?php the_field('notes', $course->ID); ?></p>

                </div>
                <div style="clear:both;"></div>
            </div>

            <hr>
      		
      	</header><!-- .entry-header -->

      	<div class="entry-content">
					
					<?php if($slideshow = get_field('slideshow')): 
					    $first_image_thumb = wp_get_attachment_image_src( $slideshow[0]['image'], 'thumbnail' );
					    $first_image = wp_get_attachment_image_src( $slideshow[0]['image'], 'full' );
					  ?>
					<div class="course-take-slideshow">
					  
					  <a href="<?php echo $first_image[0]; ?>" data-ob="lightbox[<?php echo $post->post_name; ?>]" data-ob_caption="<?php echo $slideshow[0]['description'] ?>">
					    <img src="<?php echo $first_image_thumb[0]; ?>">
					  </a>
					  
					  <?php foreach($slideshow as $index => $slide): 
					    
					      $image = wp_get_attachment_image_src( $slide['image'], 'full' );
					  ?>
					    <a class="more-link" href="<?php echo $image[0]; ?>" data-ob="lightbox[<?php echo $post->post_name; ?>]" data-ob_caption="<?php echo $slide['description'] ?>"><?php if($index == 0) :?><strong>View Slideshow</strong><?php endif; ?></a>
					  <?php endforeach; ?>
						
					</div>
					<?php endif; ?>
					
      		<?php the_content(); ?>
      		
      	</div><!-- .entry-content -->
        
      	<footer class="entry-meta">
      	  <?php include('social-snippet.php'); ?>
      		<?php edit_post_link( __( 'Edit', 'golftripster' ), '<span class="edit-link">', '</span>' ); ?>
      	</footer><!-- .entry-meta -->
      </article><!-- #post-## -->
      

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() )
					comments_template();
			?>

		<?php endwhile; // end of the loop. ?>
                </div> <!-- .page-inner-content -->
            </div> <!-- .border -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>