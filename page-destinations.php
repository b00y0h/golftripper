<?php
/**
 * The template for displaying all destinations
 *
 *
 * @package Golf Tripster
 */

get_header();

global $post;
?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content destination-content" role="main">

      <div class="destination-inner-content  inset-border triangle">
        <div class="border cf">
          
          <?php while ( have_posts() ) : the_post(); ?>

          <h1 class="destination-name"><?php the_title(); ?></h1>

          <div class="destinations-introduction">
            <?php the_content(); ?>
          </div>

          <?php
            $destinations = get_terms('destinations', array(
              'orderby' => "RAND"
            ));
          ?>
          
          <div class="destinations-wrapper">
            
            <?php if($destinations && count($destinations) > 0): ?>
              
              <div class="gs gs-to-4">
              
              <?php 
              $i = 0;
              foreach($destinations as $destination): ?>
              
                <?php if($i % 4 == 0 && $i!=0): ?>
                </div>
                <div class="gs gs-to-4">
                
                <?php endif; ?>
                <div class="destination-block gs">
                  <?php
                  $slide_thumbnail = get_field('slider_thumbnail', 'destinations_' . $destination->term_id);
                  ?>
                  <div class="destination-image-title-wrapper">
                    <a href="<?php echo get_term_link($destination); ?>">
                      <img src="<?php echo $slide_thumbnail; ?>">
                      <div class="destination-image-title"><?php echo $destination->name; ?></div>
                    </a>
                  </div>
                </div>
                
              <?php $i++; endforeach; ?>
              
              </div>
              
            <?php endif;?>
              
              
          </div>
          
          <?php endwhile; ?>
          <div style="clear:both;"></div>
          
        </div>
			</div><!-- .destination-content -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
