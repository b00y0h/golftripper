(function($){

	$(document).ready(function(){

		$("#menu-main-menu > li > a").on('click',function(e){
					if($(this).next(".sub-menu").hasClass('toggled-on')){
						if($(this).attr('href')==="#"){
							e.preventDefault();
							$(this).next(".sub-menu").removeClass('toggled-on');
						}
					}
					else{
						if($(this).next(".sub-menu").length > 0){
							$(this).next(".sub-menu").addClass('toggled-on');
							e.preventDefault();
						}						
					}
		});


		// oB.settings.addThis = false;
		$("#home-slider").nivoSlider({
			effect: 'fade',
			controlNavThumbs: true,
			pauseTime: 6000,
			afterLoad: function(){
				$('.nivo-controlNav').appendTo('.carousel.center');
			}
		});

		$(".nivo-control").on('click', function(){
			window.location.href = $("#home-slider > a").eq($(this).attr('rel')).attr('href');
		});

		$("#destination-slider").nivoSlider({
			effect: 'fade',
			controlNav: false,
			pauseTime: 6000
		});

		$(".entry-content a > img").each(function(){
			var anchor = $(this).parent();
			anchor.attr('data-ob','lighbox[post_gallery]');
			anchor.addClass('post-gallery-lightbox');
			$("a.post-gallery-lightbox").orangeBox();
			$(document).trigger('oB_ready');
		});

		// $( '.navigation-main li:has(ul)' ).doubleTapToGo();


    }); // .ready

$(window).load(function(){
	$(".nivo-control").each(function(){
		var name = $("#home-slider a.slider-image:eq("+ $(this).attr('rel') +") img").attr('title');
		$(this).append('<div class="thumb-label">'+name+'</div>');
	});
    // make colums equal height.
	$(".equalHeight").equalize({reset: true, children: 'article .border'});
});

})(jQuery);