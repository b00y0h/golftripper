<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package Golf Tripster
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="icon" type="image/ico" href="<?php bloginfo('template_directory'); ?>/images/golftripper-favicon.ico">
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<link href="<?php echo get_template_directory_uri(); ?>/ie.css" media="screen, projection" rel="stylesheet" type="text/css" />
<![endif]-->

<?php wp_head(); ?>
<?php wp_enqueue_script("jquery"); ?>
<script type="text/javascript">
  (function($){
    $(window).load(function(){
      $(".center-vertical").each(function(){
        var this_height = $(this).height();
          //console.log('this height' + this_height);
        var parent_height = $(this).parents('.center-vertical-wrapper').height();
          //console.log('parent height: ' + parent_height);
        var margin_half = (parent_height-this_height);
          //console.log('parent minus this: ' + margin_half);
        if( margin_half > 2){
          $(this).css('margin-top', (margin_half/2) + 'px');
          //console.log('new margin top: ' + $(this).css('margin-top'))
        }
      })
    })

  })(jQuery);
</script>
<!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>

<body <?php body_class(); ?>>
<?php do_action( 'before' ); ?>
<header id="masthead" class="site-header" role="banner">
    <div class="stripe-container"></div>
    <div class="center">
        <div class="inside">
    		<div class="site-branding">
    			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
    			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
    		</div>
            <div class="social-links">
                <a href="/feeds/rss" class="icon-rss">RSS</a>
                <a href="https://twitter.com/GolfTripper" class="icon-twitter">Twitter</a>
                <a href="https://www.facebook.com/pages/Golf-Tripper/512678492101292?ref=hl" class="icon-facebook">Facebook</a>
                <a href="http://instagram.com/golftripper/" class="icon-instagram">Instagram</a>
                <a href="/contact" class="icon-email">Contact</a>
            </div>

        </div>
    </div>
</header><!-- #masthead -->
<div id="sticky-header-shim"></div>
<div class="navigation-wrapper sticky">
    <nav id="site-navigation" class="navigation-main" role="navigation">
        <div class="center">
            <div class="inside">
                <h1 class="menu-toggle"><?php _e( 'Menu', 'golftripster' ); ?></h1>
                <div class="screen-reader-text skip-link"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'golftripster' ); ?>"><?php _e( 'Skip to content', 'golftripster' ); ?></a></div>


                <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
            <div id="nav-search" class="nav-search">
                    <?php get_search_form(); ?>
            </div>
            </div>
        </div>



    </nav><!-- #site-navigation -->
</div>
<div id="page" class="hfeed site">

<?php if(is_front_page() ) { ?>

<?php
  $destinations = get_terms('destinations', array('hide_empty'=>false));
	shuffle($destinations);
?>

<div class="slider-wrapper">
    <div class="slider center site-main slider-wrapper">
        <div id="home-slider" class="nivoSlider">
          <?php
            foreach($destinations as $destination):
              $slide_image = get_field('slider_image', 'destinations_' . $destination->term_id);
              $slide_thumbnail = get_field('slider_thumbnail', 'destinations_' . $destination->term_id);
              $destination_url = get_term_link($destination->slug, 'destinations');
              if(get_field('homepage_slider', 'destinations_' . $destination->term_id) && $slide_image):

          ?>
            <a class="slider-image" href="<?php echo $destination_url ?>"><img src="<?php echo $slide_image; ?>" alt="" data-thumb="<?php echo $slide_thumbnail; ?>" title="<?php echo $destination->name; ?>"></a>
            <?php

              endif;
            endforeach;
            ?>
        </div>
    </div>
    <div class="carousel-wrapper">
        <div class="carousel center">
        </div>
    </div>
</div>

<?php } ?>

	<div id="main" class="cf site-main center">
        <div class="inside">
