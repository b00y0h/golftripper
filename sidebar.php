<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package Golf Tripster
 */
?>
<div id="secondary" class="widget-area" role="complementary">
  <?php do_action( 'before_sidebar' ); ?>
  <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>

  <aside class="widget widget_widget-search">
    <div id="widget-search" class="widget-search">
      <?php get_search_form(); ?>
    </div>
  </aside>

  <aside class="widget widget_stay_connected">
    <div id="mc_embed_signup">
      <h4 class="title">News and Updates</h4>

      <form action="http://golftripper.us7.list-manage2.com/subscribe/post?u=2181d60de8e80677729359ce7&amp;id=76156ecd72" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
        <div class="mc-field-group mc-input-outline">
          <input type="email" onblur="if (this.value == '') {this.value ='enter your email';}" onfocus="if (this.value == 'enter your email') {this.value = '';}" value="enter your email" name="EMAIL" class="required email" id="mce-EMAIL" required>

          <input  id="mc-embedded-subscribe" class="newslettersubmit" src="<?php echo get_template_directory_uri(); ?>/images/signup.gif" type="image" alt="Submit">
        </div>
      </form>
      <div id="mce-responses" class="clear">
        <div class="response" id="mce-error-response" style="display:none"></div>
        <div class="response" id="mce-success-response" style="display:none"></div>
      </div>
    </div>
    <script type="text/javascript" charset="utf-8" src="<?php bloginfo('template_directory'); ?>/js/mailchimp-widget-ck.js"></script>

  </aside>
  
  <?php if(get_field('enable_custom_widget','options')) : ?>
    <aside id="custom-widget" class="widget widget_interview_top inset-border triangle">
      <div class="border">
       <h4 class="title"><?php echo (get_field('custom_widget_title', 'options')) ? get_field('custom_widget_title','options') : 'Coming Soon'; ?></h4>
       <?php if(get_field('custom_widget_content','options')): ?>
       <ul>
         <?php foreach(get_field('custom_widget_content', 'options') as $content): ?>
         <li style="list-style-type:none;font-size:12px;margin-bottom:5px;">
           <span style="color:<?php echo $content['title_color']; ?>;font-weight:bold;"><?php echo $content['title']; ?></span><br>
           <?php echo $content['text']; ?>
         </li>
         <?php endforeach; ?>
        </ul>
        <?php endif; ?>
      </div>
    </aside>
  <?php endif; ?>

  <?php if(is_single() && in_category('interviews')): ?>

  <?php if (get_field('top_10_courses')): ?>

  <aside id="top-rankings" class="widget widget_interview_top inset-border triangle">
    <div class="border">
     <h4 class="title"><?php echo (get_field('top_10_title')) ? get_field('top_10_title') : 'Top 10 Courses'; ?></h4>
     <ol class="rankings">
       <?php foreach(get_field('top_10_courses') as $course): ?>
       <li>
         <?php if($course['course_review']): ?>
         <a href="<?php echo get_permalink($course['course_review']); ?>">
           <?php echo $course['course_name']; ?>
         </a><br>
       <?php else: ?>
       <?php echo $course['course_name']; ?><br>
     <?php endif; ?>
     <span class="subtext"><?php echo $course['city_state']; ?></span>
   </li>
 <?php endforeach; ?>
</ol>
</div>
</aside>
<?php endif ?>


<?php if (get_field('sidebar_widgets')): ?>

 <?php foreach(get_field('sidebar_widgets') as $widget): ?>

 <aside id="top-rankings" class="widget widget_rankings inset-border triangle">
   <div class="border">
    <h4 class="title"><?php echo $widget['title']; ?></h4>
    <div class="interview-widget-content">
      <?php echo $widget['content']; ?>
    </div>
  </div>
</aside>

<?php endforeach; ?>

<?php endif; ?>


<?php endif; ?>

<?php
if($post->post_type != 'top-100' && !(is_single() && in_category('interviews')) ):
  $sidebar_top100_list = get_random_top100();
$rankings = get_top100_rankings(10, $sidebar_top100_list->ID);
$course_rankings = get_course_by_ranking($sidebar_top100_list->post_name);

?>
<aside id="top-rankings" class="widget widget_rankings inset-border triangle">
  <div class="border">
   <h4 class="title">Top 100 Rankings</h4>
   <h5 class="widget-subtitle"><?php echo get_the_title($sidebar_top100_list->ID); ?> (<?php the_field('years', $sidebar_top100_list->ID); ?>)</h5>
   <ol class="rankings">
    <?php foreach($rankings as $rank=>$data):
    $reviewed_course = array_search($rank, $course_rankings);
    ?>
    <li>
      <?php if($reviewed_course): ?>
      <a href="<?php echo get_permalink($reviewed_course); ?>">
        <?php echo $data['course']; ?>
      </a><br>
    <?php else: ?>
    <?php echo $data['course']; ?><br>
  <?php endif; ?>
  <span class="subtext"><?php echo $data['info']; ?></span>
</li>
<?php endforeach; ?>
</ol>

<a href="<?php echo get_permalink($sidebar_top100_list->ID); ?>" class="more-link">More &gt;</a>
<hr class='grey-border'>
<div class="top-100-bug"><a href="/top-100"><span class="bug"><img src="<?php echo get_template_directory_uri(); ?>/images/top-100-bug.png" alt="top 100 golf courses"></span><p class="center more-link">View All<br>Top 100 Lists &gt;</p></a></div>

</div>
</aside>
<?php endif; ?>

<?php if(get_field('enable_sidebar_ad', 'options') && get_field('sidebar_ad_image', 'options')): ?>
 <aside class="widget widget_ad triangle inset-border">
   <div class="border">
    <a href="<?php the_field('sidebar_ad_link', 'options'); ?>">
     <img src="<?php the_field('sidebar_ad_image', 'options'); ?>" alt="">
   </a>
 </div>
</aside>
<?php endif; ?>

<?php if(is_tax('destinations') || is_object_in_term($post->ID, 'destinations')):
$tax_query = get_query_var('tax_query');
if(is_tax('destinations')){
  $destination = get_term($tax_query[0]['terms'], 'destinations');
}
elseif(is_object_in_term($post->ID, 'destinations')){
  $terms = wp_get_object_terms($post->ID, 'destinations');
  $destination = get_term($terms[0]->term_id, 'destinations');
}
if($destination && $tripper_url = get_field('tripper_destination_guide', 'destinations_'.$destination->term_id)):
  ?>
<aside id="tripster-graphics" class="widget widget_graphics inset-border">
  <div class="border">
    <a href="<?php echo $tripper_url; ?>" target="_blank">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tripper-destination-ad.jpg" alt="">
    </a>
  </div>
</aside>
<?php endif; endif; ?>


<?php endif; // end sidebar widget area ?>
	</div><!-- #secondary -->