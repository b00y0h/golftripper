<?php
/**
 * The home page template file.
 *
 * @package Golf Tripster
 */

get_header();
global $query_string;
?>

    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">

  <?php

  $latest_cat_post = new WP_Query( array('post_type'=>'post', 'order'=>'desc', 'posts_per_page' => 1, 'meta_query'=>array(array('key'=>'feature_post','value'=>'1'),array('key'=>'homepage_headline','value'=>'1'))));

  if( $latest_cat_post->have_posts() ) : while( $latest_cat_post->have_posts() ) : $latest_cat_post->the_post();
		$featured_post_id = get_the_id();

?>

  <!-- Article Block -->
  <article typeof="Article" class="home-archive full feature inset-border triangle">
    <div class="border">
      <header>
        <h3 class="title"><a>Featured</a></h3>
        <h1 class="title main-home-article"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
      </header>

      <div class="image-feat">
        <a href="<?php the_permalink(); ?>">
        <?php if ( get_field('headline_image') ): ?>
           <img src="<?php the_field('headline_image') ?>" width="663" height="260" property="thumbnailUrl">
        <?php else: ?>
          <img src="http://dummyimage.com/663x260/ccc/000&amp;text=Default+Feature+Post+Image"  width="663" height="260" alt="" property="thumbnailUrl">
        <?php endif; ?>
        </a>
      </div>

      <div class="content" property="articleBody">
        <p><?php echo get_the_excerpt(); ?> <a href="<?php the_permalink(); ?>" class="more-link">More&nbsp;&gt;</a></p>
      </div>

    </div>
  </article>

  <?php endwhile; endif; ?>

  <?php
  //$posts = query_posts($query_string.'&posts_per_page=6&order=DESC&cat=-18&meta_key=feature_post&meta_value=1');

	$posts = new WP_Query(array(
			'posts_per_page' => 6,
			'order' => 'DESC',
			'cat' => '-18',
			'meta_key' => 'feature_post',
			'meta_value' => '1',
			'post__not_in' => array($featured_post_id)
	));
  $i=1; ?>
  <?php if ($posts->have_posts()) : while ($posts->have_posts()) : $posts->the_post();

      $category = get_the_category();
    ?>

    <?php if($i%2!=0): ?>
      <div class="ac two-column image-banner image-border-light gs-to-2 cf equalHeight">
    <?php endif; ?>

    <!-- Article Block -->
    <article id="post-<?php the_ID(); ?>" typeof="Article" class="gs home-archive inset-border triangle">
    <div class="border">
      <header>
        <h3 class="title"><a href="<?php echo get_term_link($category[0]); ?>"><?php echo $category[0]->cat_name; ?></a></h3>
        <h4 class="subtitle" property="alternativeHeadline"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
      </header>


      <div class="image-feat">
        <a href="<?php the_permalink(); ?>">

          <?php if (has_post_thumbnail( $post->ID ) ):
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
             <img src="<?php echo $image[0]; ?>" width="306" height="210" property="thumbnailUrl" class="standard-article-image">
          <?php else: ?>
             <img src="http://dummyimage.com/306x210/ccc/000&amp;text=Default+Post+Image" width="306" height="210" property="thumbnailUrl">
          <?php endif; ?>

        </a>
      </div>
      <div class="content" property="articleBody">

          <?php echo wp_trim_words(get_the_excerpt(), 52, ' '); ?>
          <a class="more-link" href="<?php the_permalink(); ?>">More&nbsp;&gt;</a>

      </div>
    </div>
    </article>

   <?php if($i%2==0): ?>
      </div>
   <?php endif; ?>

  <?php $i++; endwhile; endif;  ?>




<div class="ac two-column gs-to-2 no-border">

  <!-- Article Block -->

  <article typeof="Article" class="gs home-archive">

  <div class="content" property="articleBody">
    <?php the_field('left_block', 'options'); ?>
  </div>
  </article>

  <!-- Article Block -->

  <article typeof="Article" class="gs home-archive">

  <div class="content" property="articleBody">
     <?php the_field('right_block','options'); ?>
  </div>
  </article>

  <!-- Article Block -->

</div>






        </div><!-- #content -->
    </div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>